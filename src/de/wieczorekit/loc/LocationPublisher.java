/**
 * handyortung
 * 30.11.2010 | 12:09:50
 */
package de.wieczorekit.loc;

import static de.wieczorekit.loc.LocationUtil.getBestProvider;
import static de.wieczorekit.loc.LocationUtil.isBetterLocation;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import de.wieczorekit.pub.MessagePublisher;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 */
public class LocationPublisher {
    private final Context context;

    public LocationPublisher(final Context context) {
	this.context = context;
    }

    /**
     * Requests all available provider for location updates in order to publish the most accurate one via
     * messagePublisher.
     * 
     * @param messagePublisher
     *            the publisher to use
     * @author Marcel Wieczorek
     */
    public void publishLocation(final MessagePublisher messagePublisher) {
	final Handler handler = new Handler(Looper.getMainLooper());
	handler.post(new Runnable() {

	    /*
	     * (non-Javadoc)
	     * @see java.lang.Runnable#run()
	     */
	    @Override
	    public void run() {
		Location location = null;
		// acquire a reference to the system Location Manager
		final LocationManager locationManager = (LocationManager) LocationPublisher.this.context.getSystemService(Context.LOCATION_SERVICE);
		// acquire available providers
		final List<String> enabledProviders = locationManager.getProviders(true);
		if (enabledProviders != null && !enabledProviders.isEmpty()) {
		    final List<PublishLocationListener> locationListeners = new ArrayList<PublishLocationListener>(enabledProviders.size());
		    for (final String provider : enabledProviders) {
			Log.i(getClass().getName(), "acquired provider: " + provider);
			// obtain last known location from provider
			final Location lastKnownLocation = locationManager.getLastKnownLocation(provider);
			Log.i(getClass().getName(), "last known location: " + lastKnownLocation);
			// define a listener that responds to location updates
			final PublishLocationListener publishLocationListener = new PublishLocationListener(lastKnownLocation);
			Log.i(getClass().getName(), "start requesting location updates...");
			// register the listener with the Location Manager to receive location updates
			locationManager.requestLocationUpdates(provider, 0, 0, publishLocationListener);
			locationListeners.add(publishLocationListener);
		    }

		    int i = 0;
		    while (true) {
			try {
			    Thread.sleep(2000L);
			} catch (final InterruptedException e) {
			    Log.e(getClass().getName(), e.getMessage(), e);
			} finally {
			    if (++i >= 20) {
				// find most accurate location
				for (final PublishLocationListener publishLocationListener : locationListeners) {
				    locationManager.removeUpdates(publishLocationListener);
				    if (isBetterLocation(publishLocationListener.getCurrentLocation(), location)) {
					location = publishLocationListener.getCurrentLocation();
				    }
				}
				break;
			    }
			}
		    }

		    messagePublisher.publish(location);
		}
	    }
	});
    }

    /**
     * Requests only the best available provider for location updates in order to publish the most accurate one via
     * messagePublisher.
     * 
     * @param messagePublisher
     *            the publisher to use
     * @author Marcel Wieczorek
     */
    public void publishLocationFromBestProvider(final MessagePublisher messagePublisher) {
	final Handler handler = new Handler(Looper.getMainLooper());
	handler.post(new Runnable() {

	    /*
	     * (non-Javadoc)
	     * @see java.lang.Runnable#run()
	     */
	    @Override
	    public void run() {
		Location location = null;
		// acquire a reference to the system Location Manager
		final LocationManager locationManager = (LocationManager) LocationPublisher.this.context.getSystemService(Context.LOCATION_SERVICE);
		// acquire best available provider
		final String bestProvider = getBestProvider(locationManager);
		Log.i(getClass().getName(), "acquired best provider: " + bestProvider);
		// obtain last known location from provider
		final Location lastKnownLocation = locationManager.getLastKnownLocation(bestProvider);
		Log.i(getClass().getName(), "last known location: " + lastKnownLocation);
		// define a listener that responds to location updates
		final PublishLocationListener publishLocationListener = new PublishLocationListener(lastKnownLocation);
		Log.i(getClass().getName(), "start requesting location updates...");
		// register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(bestProvider, 0, 0, publishLocationListener);

		int i = 0;
		while (true) {
		    try {
			Thread.sleep(2000L);
		    } catch (final InterruptedException e) {
			Log.e(getClass().getName(), e.getMessage(), e);
		    } finally {
			if (++i >= 20) {
			    locationManager.removeUpdates(publishLocationListener);
			    break;
			}
		    }
		}

		location = publishLocationListener.getCurrentLocation();
		messagePublisher.publish(location);
	    }
	});
    }
}
