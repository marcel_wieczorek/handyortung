/**
 * handyortung
 * 30.11.2010 | 14:53:12
 */
package de.wieczorekit.loc;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import de.wieczorekit.pub.MessagePublisher;
import de.wieczorekit.pub.SmsPublisher;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 */
public class PublishLocationTask extends AsyncTask<Context, Void, Void> {
    private final String recipientAddress;
    private final boolean useBestProviderOnly;

    public PublishLocationTask(final String recipientAddress, final boolean useBestProviderOnly) {
	this.recipientAddress = recipientAddress;
	this.useBestProviderOnly = useBestProviderOnly;
    }

    /*
     * (non-Javadoc)
     * @see android.os.AsyncTask#doInBackground(Params[])
     */
    @Override
    protected Void doInBackground(final Context... params) {
	if (params != null && params.length > 0) {
	    Log.i(getClass().getName(), "start to request for location updates");
	    final Context context = params[0];
	    final LocationPublisher locationPublisher = new LocationPublisher(context);
	    final MessagePublisher messagePublisher = new SmsPublisher(context, this.recipientAddress);
	    if (this.useBestProviderOnly) {
		locationPublisher.publishLocationFromBestProvider(messagePublisher);
	    } else {
		locationPublisher.publishLocation(messagePublisher);
	    }
	}
	return null;
    }

    /*
     * (non-Javadoc)
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute(final Void result) {
	Log.i(getClass().getName(), "execution of location task has finished");
    }

    /*
     * (non-Javadoc)
     * @see android.os.AsyncTask#onPreExecute()
     */
    @Override
    protected void onPreExecute() {
	Log.i(getClass().getName(), "going to execute location task");
    }

}
