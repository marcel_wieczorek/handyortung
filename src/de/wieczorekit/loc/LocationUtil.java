/**
 * handyortung
 * 30.11.2010 | 16:39:10
 */
package de.wieczorekit.loc;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 */
public class LocationUtil {
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    public static final String getBestProvider(final LocationManager locationManager) {
	final Criteria criteria = new Criteria();
	criteria.setAccuracy(Criteria.ACCURACY_FINE);
	criteria.setAltitudeRequired(false);
	criteria.setBearingRequired(false);
	criteria.setCostAllowed(true);
	criteria.setPowerRequirement(Criteria.POWER_LOW);

	return locationManager.getBestProvider(criteria, true);
    }

    /**
     * Determines whether one Location reading is better than the current Location fix.
     * 
     * @param location
     *            the new Location that you want to evaluate
     * @param currentBestLocation
     *            the current Location fix, to which you want to compare the new one
     */
    public static final boolean isBetterLocation(final Location location, final Location currentBestLocation) {
	if (currentBestLocation == null) {
	    // a new location is always better than no location
	    return true;
	}

	// check whether the new location fix is newer or older
	final long timeDelta = location.getTime() - currentBestLocation.getTime();
	final boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
	final boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
	final boolean isNewer = timeDelta > 0;

	// if it's been more than two minutes since the current location, use the new location because the user has
	// likely moved
	if (isSignificantlyNewer) {
	    return true;
	    // if the new location is more than two minutes older, it must be worse
	} else if (isSignificantlyOlder) {
	    return false;
	}

	// check whether the new location fix is more or less accurate
	final int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	final boolean isLessAccurate = accuracyDelta > 0;
	final boolean isMoreAccurate = accuracyDelta < 0;
	final boolean isSignificantlyLessAccurate = accuracyDelta > 200;// more than 200 meters

	// check if the old and new location are from the same provider
	final boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

	// determine location quality using a combination of timeliness and accuracy
	if (isMoreAccurate) {
	    return true;
	} else if (isNewer && !isLessAccurate) {
	    return true;
	} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	    return true;
	}
	return false;
    }

    /**
     * Checks whether two providers are the same.
     */
    public static final boolean isSameProvider(final String provider, final String otherProvider) {
	if (provider == null) {
	    return otherProvider == null;
	}
	return provider.equals(otherProvider);
    }
}
