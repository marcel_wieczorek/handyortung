/**
 * handyortung
 * 30.11.2010 | 13:26:41
 */
package de.wieczorekit.loc;

import static de.wieczorekit.loc.LocationUtil.isBetterLocation;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 */
class PublishLocationListener implements LocationListener {
    private Location currentLocation;

    PublishLocationListener(final Location lastKnownLocation) {
	this.currentLocation = lastKnownLocation;
    }

    /**
     * @return the currentLocation
     */
    public Location getCurrentLocation() {
	return this.currentLocation;
    }

    /*
     * (non-Javadoc)
     * @see android.location.LocationListener#onLocationChanged(android.location.Location)
     */
    @Override
    public void onLocationChanged(final Location location) {
	Log.i(getClass().getName(), "got location: " + location);
	// called when a new location is found by the location provider.
	if (isBetterLocation(location, this.currentLocation)) {
	    Log.i(getClass().getName(), "found better location");
	    this.currentLocation = location;
	}
    }

    /*
     * (non-Javadoc)
     * @see android.location.LocationListener#onProviderDisabled(java.lang.String)
     */
    @Override
    public void onProviderDisabled(final String provider) {
	// do nothing

    }

    /*
     * (non-Javadoc)
     * @see android.location.LocationListener#onProviderEnabled(java.lang.String)
     */
    @Override
    public void onProviderEnabled(final String provider) {
	// do nothing

    }

    /*
     * (non-Javadoc)
     * @see android.location.LocationListener#onStatusChanged(java.lang.String, int, android.os.Bundle)
     */
    @Override
    public void onStatusChanged(final String provider, final int status, final Bundle extras) {
	// do nothing

    }

}
