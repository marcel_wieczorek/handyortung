/**
 * handyortung
 * 29.09.2010 | 20:12:11
 */
package de.wieczorekit;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * @author <a href="mailto:marcel@wieczorekit.de"><b>Marcel Wieczorek</b></a>
 * @version 1.0
 */
public class HandyortungActivity extends Activity {

    /*
     * (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.main);

	// get text view
	final TextView textView = (TextView) findViewById(R.id.TextViewHead);

	textView.append("nothing to do...");
    }

    /*
     * (non-Javadoc)
     * @see android.app.Activity#onDestroy()
     */
    @Override
    protected void onDestroy() {
	super.onDestroy();
    }

    /*
     * (non-Javadoc)
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
	super.onPause();
    }

    /*
     * (non-Javadoc)
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
	super.onResume();
    }

}