/**
 * handyortung
 * 29.11.2010 | 11:29:27
 */
package de.wieczorekit.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;

/**
 * @author <a href="mailto:marcel@wieczorekit.de"><b>Marcel Wieczorek</b></a>
 * @version 1.0
 */
public abstract class AbstractBroadcastReceiver extends BroadcastReceiver {
    protected abstract void sendResponseMessage(final Context context, final String originatingAddress) throws Exception;
}
