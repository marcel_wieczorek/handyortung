/**
 * handyortung
 * 19.11.2010 | 18:29:22
 */
package de.wieczorekit.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import de.wieczorekit.loc.PublishLocationTask;

/**
 * @author <a href="mailto:marcel@wieczorekit.de"><b>Marcel Wieczorek</b></a>
 */
public class SmsReceiver extends AbstractBroadcastReceiver {
    private static final String SMS_KEY_CONTENT = "123";

    /*
     * (non-Javadoc)
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
     */
    @Override
    public void onReceive(final Context context, final Intent intent) {
	Log.i(getClass().getName(), "received short messages...");
	// ---get the SMS message passed in---
	final Bundle bundle = intent.getExtras();
	if (bundle != null) {
	    // ---retrieve the SMS message received---
	    final Object[] pdus = (Object[]) bundle.get("pdus");
	    final SmsMessage[] msgs = new SmsMessage[pdus.length];
	    for (int i = 0; i < msgs.length; i++) {
		msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
		final String messageBody = msgs[i].getMessageBody();
		if (SMS_KEY_CONTENT.equals(messageBody)) {
		    final String originatingAddress = msgs[i].getOriginatingAddress();
		    Log.i(getClass().getName(), "forced to answer with location info due to message from: " + originatingAddress);
		    try {
			sendResponseMessage(context, originatingAddress);
		    } catch (final Exception e) {
			Log.e(getClass().getName(), e.getMessage(), e);
		    }
		    break;
		}
	    }
	}
    }

    /*
     * (non-Javadoc)
     * @see de.wieczorekit.receivers.AbstractBroadcastReceiver#sendResponseMessage(android.content.Context,
     * java.lang.String)
     */
    @Override
    protected void sendResponseMessage(final Context context, final String originatingAddress) throws Exception {
	Log.i(getClass().getName(), "sending response message...");
	final AsyncTask<Context, Void, Void> asyncTask = new PublishLocationTask(originatingAddress, false);
	asyncTask.execute(context);
    }
}
