/**
 * handyortung
 * 30.11.2010 | 13:50:21
 */
package de.wieczorekit.pub;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 */
public class SmsPublisher implements MessagePublisher {
    private final String recipientAddress;
    private final Context context;

    public SmsPublisher(final Context context, final String recipientAddress) {
	this.context = context;
	this.recipientAddress = recipientAddress;
    }

    /*
     * (non-Javadoc)
     * @see de.wieczorekit.pub.MessagePublisher#publish(android.content.Context, android.location.Location)
     */
    @Override
    public void publish(final Location location) {
	if (this.context != null && location != null) {
	    final String message = String.format("%s (%d)%naccuracy: %f%nmaps (latitude, longitude)%n%f, %f", location.getProvider(), location.getTime(), location.getAccuracy(), location.getLatitude(), location.getLongitude());
	    Log.i(getClass().getName(), "going to publish the following message: " + message);

	    final SmsManager smsManager = SmsManager.getDefault();
	    final PendingIntent pendingIntent = PendingIntent.getActivity(this.context, 0, new Intent(this.context, SmsMessage.class), 0);
	    smsManager.sendTextMessage(this.recipientAddress, null, message, pendingIntent, null);
	} else {
	    Log.e(getClass().getName(), "could not publish location " + location + " using context " + this.context);
	}
    }

}
