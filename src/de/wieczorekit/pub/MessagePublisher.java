/**
 * handyortung
 * 30.11.2010 | 13:36:29
 */
package de.wieczorekit.pub;

import android.location.Location;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 */
public interface MessagePublisher {
    void publish(Location location);
}
